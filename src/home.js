import Icon from "./images/home-image.jpg";

function component() {
  const heading = document.createElement("h1");
  heading.textContent = "The Risto";
  heading.classList.add("home-main-title");
  const paragraph = document.createElement("p");
  paragraph.textContent =
    "We strive to bring people the best food by using the freshest and best ingredients we can find. Enjoy our healthy and tasty food!";

  const containerText = document.createElement("div");
  containerText.classList.add("home-main-text");
  containerText.appendChild(heading);
  containerText.appendChild(paragraph);

  const img = new Image();
  img.classList.add("home-main-img");
  img.src = Icon;
  img.alt = "Bowl of roasted vegetables";

  const container = document.createElement("div");
  container.appendChild(containerText);
  container.appendChild(img);

  container.classList.add("container");
  container.classList.add("content");

  const main = document.createElement("main");
  main.appendChild(container);

  return main;
}

export default function homeMain() {
  document.querySelector("main").replaceWith(component());
}
