import "./css/style.css";
import homeMain from "./home.js";
import menuMain from "./menu.js";
import contactMain from "./contact.js";

function showPage(event) {
  const lastPart = window.location.hash.slice(1);

  switch (lastPart) {
    case "home":
      homeMain();
      break;
    case "menu":
      menuMain();
      break;
    case "contact":
      contactMain();
      break;
    default:
      homeMain();
      break;
  }
}

function init() {
  document.querySelector(".logo").addEventListener("click", () => {
    history.pushState(null, null, "#home");
    homeMain();
  });

  document.querySelector(".home").addEventListener("click", () => {
    history.pushState(null, null, "#home");
    homeMain();
  });
  document.querySelector(".menu").addEventListener("click", () => {
    history.pushState(null, null, "#menu");
    menuMain();
  });

  document.querySelector(".contact").addEventListener("click", () => {
    history.pushState(null, null, "#contact");
    contactMain();
  });

  ["load", "popstate"].forEach((ev) => {
    window.addEventListener(ev, showPage);
  });

  homeMain();
}

init();
