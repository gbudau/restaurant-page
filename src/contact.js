const contact_fields = [
  {
    name: "CALL US",
    info: "1 (234) 567-891, 1 (234) 987-654",
  },
  {
    name: "LOCATION",
    info: "121 Rock Street, 21 Avenue, New York, NY 92103-9000",
  },
  {
    name: "BUSINESS HOURS",
    info: "Monday - Friday ..... 10 am - 8 pm, Sat - Sun ..... closed",
  },
];

function createContactFields(field) {
  const name = document.createElement("h2");
  name.textContent = field.name;

  const info = document.createElement("p");
  info.textContent = field.info;

  return [name, info];
}

function component() {
  const contactUs = document.createElement("h1");
  contactUs.classList.add("contact-us");
  contactUs.textContent = "Contact Us";

  const container = document.createElement("div");
  container.classList.add("content");
  container.classList.add("contact-container");

  container.appendChild(contactUs);

  for (let field of contact_fields) {
    container.append(...createContactFields(field));
  }

  const main = document.createElement("main");
  main.appendChild(container);

  return main;
}

export default function contactMain() {
  document.querySelector("main").replaceWith(component());
}
