const sandwiches = [
  {
    name: "Solstice Tofu Sandwich",
    description:
      "Grilled tofu topped with sun-dried tomatoes, pickles, spinach and onions and on a wheat roll. Served with a side of vegan mayo",
    price: 10.95,
  },
  {
    name: "House-made Veggie Burger",
    description:
      "Quinoa, mushroom and oat burger with a touch of smoked paprika, served on a wheat roll with lettuce, tomato and onions. Your choice of ketchup, mustard or vegan mayo on the side.",
    price: 11.95,
  },
  {
    name: "Gluten-free Veggie Wrap",
    description:
      "Hummus, spinach, tomatoes and black olives stuffed into a gluten-free herb wrap.",
    price: 10.95,
  },
  {
    name: "Bahn Mi",
    description:
      "Lemongrass tofu grilled and served on a baguette with pickled vegetables, cilantro and spicy vegan mayo.",
    price: 11.95,
  },
  {
    name: "Falafel",
    description:
      "Baked chickpea patties, topped with chopped cucumber, tomatoes, red onions and tahini sauce on a wheat pita",
    price: 10.95,
  },
  {
    name: 'Philly "Cheesesteak"',
    description:
      'Chopped seitan steak, topped with pimento "cheez" and fried onions, peppers and mushrooms on a wheat roll.',
    price: 11.95,
  },
  {
    name: "Grilled veggie",
    description:
      "Grilled zucchini and eggplant with sauteed spinach. Served on sourdough bread with cilantro pesto.",
    price: 10.95,
  },
  {
    name: "The Reuben",
    description:
      "House seitan, sauerkraut, and dill pickle, topped with vegan cheese and thousand island dressing on toasted rye.",
    price: 11.95,
  },
];

const entrees = [
  {
    name: "Red Curry Platter",
    description:
      "Red curry chock full of seasonal vegetables, served with basmati rice and home-made coleslaw.",
    price: 12.95,
  },
  {
    name: "Rice And Beans Bowl",
    description:
      "Basmati rice and stewed black and red beans, topped with home-made salsa, cilantro, black olives and avocado.",
    price: 11.95,
  },
  {
    name: "Tofu Scramble",
    description:
      "Spiced tofu scramble with spicy seitan sausage, served with homefries and ketchup.",
    price: 12.95,
  },
  {
    name: "Twice Fried Black Beans And Green Rice",
    description:
      "Mashed, spicy black beans and green rice, served with picked onions, salsa and avocado.",
    price: 12.95,
  },
  {
    name: "Tempeh Tacos",
    description:
      "Smoky tempeh, served in fresh corn tortillas and topped with black beans, salsa and avocado.",
    price: 13.95,
  },
  {
    name: "Black Bean Tostada",
    description:
      "Black beans, shredded cabbage, salsa and guacamole on top of a crisp tortilla shell.",
    price: 10.95,
  },
  {
    name: "Autumn Bowl",
    description:
      "Butternut squash, sauteed kale, white beans, and picked onions served on a bed of basmati rice.",
    price: 13.95,
  },
];

function createMenus(menus) {
  const nodes = [];

  for (let menu of menus) {
    const name = document.createElement("p");
    name.textContent = menu.name.toUpperCase();
    name.classList.add("menu-name");

    const price = document.createElement("p");
    price.textContent = menu.price;
    price.classList.add("menu-price");

    const menuInfo = document.createElement("div");
    menuInfo.classList.add("menu-info");
    menuInfo.appendChild(name);
    menuInfo.appendChild(price);

    const description = document.createElement("p");
    description.textContent = menu.description;
    description.classList.add("menu-description");

    const menuContainer = document.createElement("div");
    menuContainer.appendChild(menuInfo);
    menuContainer.appendChild(description);
    nodes.push(menuContainer);
  }

  return nodes;
}

function component() {
  const container = document.createElement("div");

  container.classList.add("menu-container");
  container.classList.add("content");

  container.append(...createMenus(sandwiches));
  container.append(...createMenus(entrees));

  const main = document.createElement("main");
  main.appendChild(container);

  return main;
}

export default function menuMain() {
  document.querySelector("main").replaceWith(component());
}
