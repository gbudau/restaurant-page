/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!*********************!*\
  !*** ./src/menu.js ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ menuMain)
/* harmony export */ });
const sandwiches = [
  {
    name: "Solstice Tofu Sandwich",
    description:
      "Grilled tofu topped with sun-dried tomatoes, pickles, spinach and onions and on a wheat roll. Served with a side of vegan mayo",
    price: 10.95,
  },
  {
    name: "House-made Veggie Burger",
    description:
      "Quinoa, mushroom and oat burger with a touch of smoked paprika, served on a wheat roll with lettuce, tomato and onions. Your choice of ketchup, mustard or vegan mayo on the side.",
    price: 11.95,
  },
  {
    name: "Gluten-free Veggie Wrap",
    description:
      "Hummus, spinach, tomatoes and black olives stuffed into a gluten-free herb wrap.",
    price: 10.95,
  },
  {
    name: "Bahn Mi",
    description:
      "Lemongrass tofu grilled and served on a baguette with pickled vegetables, cilantro and spicy vegan mayo.",
    price: 11.95,
  },
  {
    name: "Falafel",
    description:
      "Baked chickpea patties, topped with chopped cucumber, tomatoes, red onions and tahini sauce on a wheat pita",
    price: 10.95,
  },
  {
    name: 'Philly "Cheesesteak"',
    description:
      'Chopped seitan steak, topped with pimento "cheez" and fried onions, peppers and mushrooms on a wheat roll.',
    price: 11.95,
  },
  {
    name: "Grilled veggie",
    description:
      "Grilled zucchini and eggplant with sauteed spinach. Served on sourdough bread with cilantro pesto.",
    price: 10.95,
  },
  {
    name: "The Reuben",
    description:
      "House seitan, sauerkraut, and dill pickle, topped with vegan cheese and thousand island dressing on toasted rye.",
    price: 11.95,
  },
];

const entrees = [
  {
    name: "Red Curry Platter",
    description:
      "Red curry chock full of seasonal vegetables, served with basmati rice and home-made coleslaw.",
    price: 12.95,
  },
  {
    name: "Rice And Beans Bowl",
    description:
      "Basmati rice and stewed black and red beans, topped with home-made salsa, cilantro, black olives and avocado.",
    price: 11.95,
  },
  {
    name: "Tofu Scramble",
    description:
      "Spiced tofu scramble with spicy seitan sausage, served with homefries and ketchup.",
    price: 12.95,
  },
  {
    name: "Twice Fried Black Beans And Green Rice",
    description:
      "Mashed, spicy black beans and green rice, served with picked onions, salsa and avocado.",
    price: 12.95,
  },
  {
    name: "Tempeh Tacos",
    description:
      "Smoky tempeh, served in fresh corn tortillas and topped with black beans, salsa and avocado.",
    price: 13.95,
  },
  {
    name: "Black Bean Tostada",
    description:
      "Black beans, shredded cabbage, salsa and guacamole on top of a crisp tortilla shell.",
    price: 10.95,
  },
  {
    name: "Autumn Bowl",
    description:
      "Butternut squash, sauteed kale, white beans, and picked onions served on a bed of basmati rice.",
    price: 13.95,
  },
];

function createMenus(menus) {
  const nodes = [];

  for (let menu of menus) {
    const name = document.createElement("p");
    name.textContent = menu.name.toUpperCase();
    name.classList.add("menu-name");

    const price = document.createElement("p");
    price.textContent = menu.price;
    price.classList.add("menu-price");

    const menuInfo = document.createElement("div");
    menuInfo.classList.add("menu-info");
    menuInfo.appendChild(name);
    menuInfo.appendChild(price);

    const description = document.createElement("p");
    description.textContent = menu.description;
    description.classList.add("menu-description");

    const menuContainer = document.createElement("div");
    menuContainer.appendChild(menuInfo);
    menuContainer.appendChild(description);
    nodes.push(menuContainer);
  }

  return nodes;
}

function component() {
  const container = document.createElement("div");

  container.classList.add("menu-container");
  container.classList.add("content");

  container.append(...createMenus(sandwiches));
  container.append(...createMenus(entrees));

  const main = document.createElement("main");
  main.appendChild(container);

  return main;
}

function menuMain() {
  document.querySelector("main").replaceWith(component());
}

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5idW5kbGUuanMiLCJtYXBwaW5ncyI6Ijs7VUFBQTtVQUNBOzs7OztXQ0RBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7Ozs7O1dDQUE7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRWU7QUFDZjtBQUNBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVzdGF1cmFudC1wYWdlL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL3Jlc3RhdXJhbnQtcGFnZS93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vcmVzdGF1cmFudC1wYWdlL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vcmVzdGF1cmFudC1wYWdlL3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vcmVzdGF1cmFudC1wYWdlLy4vc3JjL21lbnUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gVGhlIHJlcXVpcmUgc2NvcGVcbnZhciBfX3dlYnBhY2tfcmVxdWlyZV9fID0ge307XG5cbiIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJjb25zdCBzYW5kd2ljaGVzID0gW1xuICB7XG4gICAgbmFtZTogXCJTb2xzdGljZSBUb2Z1IFNhbmR3aWNoXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkdyaWxsZWQgdG9mdSB0b3BwZWQgd2l0aCBzdW4tZHJpZWQgdG9tYXRvZXMsIHBpY2tsZXMsIHNwaW5hY2ggYW5kIG9uaW9ucyBhbmQgb24gYSB3aGVhdCByb2xsLiBTZXJ2ZWQgd2l0aCBhIHNpZGUgb2YgdmVnYW4gbWF5b1wiLFxuICAgIHByaWNlOiAxMC45NSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiSG91c2UtbWFkZSBWZWdnaWUgQnVyZ2VyXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIlF1aW5vYSwgbXVzaHJvb20gYW5kIG9hdCBidXJnZXIgd2l0aCBhIHRvdWNoIG9mIHNtb2tlZCBwYXByaWthLCBzZXJ2ZWQgb24gYSB3aGVhdCByb2xsIHdpdGggbGV0dHVjZSwgdG9tYXRvIGFuZCBvbmlvbnMuIFlvdXIgY2hvaWNlIG9mIGtldGNodXAsIG11c3RhcmQgb3IgdmVnYW4gbWF5byBvbiB0aGUgc2lkZS5cIixcbiAgICBwcmljZTogMTEuOTUsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIkdsdXRlbi1mcmVlIFZlZ2dpZSBXcmFwXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkh1bW11cywgc3BpbmFjaCwgdG9tYXRvZXMgYW5kIGJsYWNrIG9saXZlcyBzdHVmZmVkIGludG8gYSBnbHV0ZW4tZnJlZSBoZXJiIHdyYXAuXCIsXG4gICAgcHJpY2U6IDEwLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJCYWhuIE1pXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkxlbW9uZ3Jhc3MgdG9mdSBncmlsbGVkIGFuZCBzZXJ2ZWQgb24gYSBiYWd1ZXR0ZSB3aXRoIHBpY2tsZWQgdmVnZXRhYmxlcywgY2lsYW50cm8gYW5kIHNwaWN5IHZlZ2FuIG1heW8uXCIsXG4gICAgcHJpY2U6IDExLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJGYWxhZmVsXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkJha2VkIGNoaWNrcGVhIHBhdHRpZXMsIHRvcHBlZCB3aXRoIGNob3BwZWQgY3VjdW1iZXIsIHRvbWF0b2VzLCByZWQgb25pb25zIGFuZCB0YWhpbmkgc2F1Y2Ugb24gYSB3aGVhdCBwaXRhXCIsXG4gICAgcHJpY2U6IDEwLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogJ1BoaWxseSBcIkNoZWVzZXN0ZWFrXCInLFxuICAgIGRlc2NyaXB0aW9uOlxuICAgICAgJ0Nob3BwZWQgc2VpdGFuIHN0ZWFrLCB0b3BwZWQgd2l0aCBwaW1lbnRvIFwiY2hlZXpcIiBhbmQgZnJpZWQgb25pb25zLCBwZXBwZXJzIGFuZCBtdXNocm9vbXMgb24gYSB3aGVhdCByb2xsLicsXG4gICAgcHJpY2U6IDExLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJHcmlsbGVkIHZlZ2dpZVwiLFxuICAgIGRlc2NyaXB0aW9uOlxuICAgICAgXCJHcmlsbGVkIHp1Y2NoaW5pIGFuZCBlZ2dwbGFudCB3aXRoIHNhdXRlZWQgc3BpbmFjaC4gU2VydmVkIG9uIHNvdXJkb3VnaCBicmVhZCB3aXRoIGNpbGFudHJvIHBlc3RvLlwiLFxuICAgIHByaWNlOiAxMC45NSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiVGhlIFJldWJlblwiLFxuICAgIGRlc2NyaXB0aW9uOlxuICAgICAgXCJIb3VzZSBzZWl0YW4sIHNhdWVya3JhdXQsIGFuZCBkaWxsIHBpY2tsZSwgdG9wcGVkIHdpdGggdmVnYW4gY2hlZXNlIGFuZCB0aG91c2FuZCBpc2xhbmQgZHJlc3Npbmcgb24gdG9hc3RlZCByeWUuXCIsXG4gICAgcHJpY2U6IDExLjk1LFxuICB9LFxuXTtcblxuY29uc3QgZW50cmVlcyA9IFtcbiAge1xuICAgIG5hbWU6IFwiUmVkIEN1cnJ5IFBsYXR0ZXJcIixcbiAgICBkZXNjcmlwdGlvbjpcbiAgICAgIFwiUmVkIGN1cnJ5IGNob2NrIGZ1bGwgb2Ygc2Vhc29uYWwgdmVnZXRhYmxlcywgc2VydmVkIHdpdGggYmFzbWF0aSByaWNlIGFuZCBob21lLW1hZGUgY29sZXNsYXcuXCIsXG4gICAgcHJpY2U6IDEyLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJSaWNlIEFuZCBCZWFucyBCb3dsXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkJhc21hdGkgcmljZSBhbmQgc3Rld2VkIGJsYWNrIGFuZCByZWQgYmVhbnMsIHRvcHBlZCB3aXRoIGhvbWUtbWFkZSBzYWxzYSwgY2lsYW50cm8sIGJsYWNrIG9saXZlcyBhbmQgYXZvY2Fkby5cIixcbiAgICBwcmljZTogMTEuOTUsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIlRvZnUgU2NyYW1ibGVcIixcbiAgICBkZXNjcmlwdGlvbjpcbiAgICAgIFwiU3BpY2VkIHRvZnUgc2NyYW1ibGUgd2l0aCBzcGljeSBzZWl0YW4gc2F1c2FnZSwgc2VydmVkIHdpdGggaG9tZWZyaWVzIGFuZCBrZXRjaHVwLlwiLFxuICAgIHByaWNlOiAxMi45NSxcbiAgfSxcbiAge1xuICAgIG5hbWU6IFwiVHdpY2UgRnJpZWQgQmxhY2sgQmVhbnMgQW5kIEdyZWVuIFJpY2VcIixcbiAgICBkZXNjcmlwdGlvbjpcbiAgICAgIFwiTWFzaGVkLCBzcGljeSBibGFjayBiZWFucyBhbmQgZ3JlZW4gcmljZSwgc2VydmVkIHdpdGggcGlja2VkIG9uaW9ucywgc2Fsc2EgYW5kIGF2b2NhZG8uXCIsXG4gICAgcHJpY2U6IDEyLjk1LFxuICB9LFxuICB7XG4gICAgbmFtZTogXCJUZW1wZWggVGFjb3NcIixcbiAgICBkZXNjcmlwdGlvbjpcbiAgICAgIFwiU21va3kgdGVtcGVoLCBzZXJ2ZWQgaW4gZnJlc2ggY29ybiB0b3J0aWxsYXMgYW5kIHRvcHBlZCB3aXRoIGJsYWNrIGJlYW5zLCBzYWxzYSBhbmQgYXZvY2Fkby5cIixcbiAgICBwcmljZTogMTMuOTUsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIkJsYWNrIEJlYW4gVG9zdGFkYVwiLFxuICAgIGRlc2NyaXB0aW9uOlxuICAgICAgXCJCbGFjayBiZWFucywgc2hyZWRkZWQgY2FiYmFnZSwgc2Fsc2EgYW5kIGd1YWNhbW9sZSBvbiB0b3Agb2YgYSBjcmlzcCB0b3J0aWxsYSBzaGVsbC5cIixcbiAgICBwcmljZTogMTAuOTUsXG4gIH0sXG4gIHtcbiAgICBuYW1lOiBcIkF1dHVtbiBCb3dsXCIsXG4gICAgZGVzY3JpcHRpb246XG4gICAgICBcIkJ1dHRlcm51dCBzcXVhc2gsIHNhdXRlZWQga2FsZSwgd2hpdGUgYmVhbnMsIGFuZCBwaWNrZWQgb25pb25zIHNlcnZlZCBvbiBhIGJlZCBvZiBiYXNtYXRpIHJpY2UuXCIsXG4gICAgcHJpY2U6IDEzLjk1LFxuICB9LFxuXTtcblxuZnVuY3Rpb24gY3JlYXRlTWVudXMobWVudXMpIHtcbiAgY29uc3Qgbm9kZXMgPSBbXTtcblxuICBmb3IgKGxldCBtZW51IG9mIG1lbnVzKSB7XG4gICAgY29uc3QgbmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIpO1xuICAgIG5hbWUudGV4dENvbnRlbnQgPSBtZW51Lm5hbWUudG9VcHBlckNhc2UoKTtcbiAgICBuYW1lLmNsYXNzTGlzdC5hZGQoXCJtZW51LW5hbWVcIik7XG5cbiAgICBjb25zdCBwcmljZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIpO1xuICAgIHByaWNlLnRleHRDb250ZW50ID0gbWVudS5wcmljZTtcbiAgICBwcmljZS5jbGFzc0xpc3QuYWRkKFwibWVudS1wcmljZVwiKTtcblxuICAgIGNvbnN0IG1lbnVJbmZvID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcbiAgICBtZW51SW5mby5jbGFzc0xpc3QuYWRkKFwibWVudS1pbmZvXCIpO1xuICAgIG1lbnVJbmZvLmFwcGVuZENoaWxkKG5hbWUpO1xuICAgIG1lbnVJbmZvLmFwcGVuZENoaWxkKHByaWNlKTtcblxuICAgIGNvbnN0IGRlc2NyaXB0aW9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7XG4gICAgZGVzY3JpcHRpb24udGV4dENvbnRlbnQgPSBtZW51LmRlc2NyaXB0aW9uO1xuICAgIGRlc2NyaXB0aW9uLmNsYXNzTGlzdC5hZGQoXCJtZW51LWRlc2NyaXB0aW9uXCIpO1xuXG4gICAgY29uc3QgbWVudUNvbnRhaW5lciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgbWVudUNvbnRhaW5lci5hcHBlbmRDaGlsZChtZW51SW5mbyk7XG4gICAgbWVudUNvbnRhaW5lci5hcHBlbmRDaGlsZChkZXNjcmlwdGlvbik7XG4gICAgbm9kZXMucHVzaChtZW51Q29udGFpbmVyKTtcbiAgfVxuXG4gIHJldHVybiBub2Rlcztcbn1cblxuZnVuY3Rpb24gY29tcG9uZW50KCkge1xuICBjb25zdCBjb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuXG4gIGNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKFwibWVudS1jb250YWluZXJcIik7XG4gIGNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKFwiY29udGVudFwiKTtcblxuICBjb250YWluZXIuYXBwZW5kKC4uLmNyZWF0ZU1lbnVzKHNhbmR3aWNoZXMpKTtcbiAgY29udGFpbmVyLmFwcGVuZCguLi5jcmVhdGVNZW51cyhlbnRyZWVzKSk7XG5cbiAgY29uc3QgbWFpbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJtYWluXCIpO1xuICBtYWluLmFwcGVuZENoaWxkKGNvbnRhaW5lcik7XG5cbiAgcmV0dXJuIG1haW47XG59XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG1lbnVNYWluKCkge1xuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwibWFpblwiKS5yZXBsYWNlV2l0aChjb21wb25lbnQoKSk7XG59XG4iXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=